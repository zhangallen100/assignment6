import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None)
    username = os.environ.get("USER", None)
    password = os.environ.get("PASSWORD", None)
    hostname = os.environ.get("HOST", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(ID INT UNSIGNED NOT NULL AUTO_INCREMENT, Year INT, Title TEXT,Director TEXT, Actor TEXT, Release_Date TEXT, Rating DOUBLE, PRIMARY KEY (ID))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("Table already exists.")
        else:
            print(err.msg)
    else:
        print("Table created.")




try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    msg = "(" + year + ", '" + title + "', '" + director + "', '"+actor + "', '"+release_date+"', "+ rating + ")"

    db, username, password, hostname = get_db_creds()

    cnx = ''
    ret_msg = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    querycur = cnx.cursor()
    querycur.execute("SELECT Title FROM movies WHERE Title ='"+title+"'")
    entries = [dict(greeting=row[0]) for row in querycur.fetchall()]
    if len(entries)==0:
        cur = cnx.cursor()
        cur.execute("INSERT INTO movies (Year, Title, Director, Actor, Release_Date, Rating) values " + msg)
        cnx.commit()
        ret_msg = "Movie " + title + " successfully inserted"
    else:
        ret_msg = "Movie " + title + " could not be inserted- Movie already exists in database"

    return hello(ret_msg)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    ret_msg = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    
    querycur = cnx.cursor()
    querycur.execute("SELECT Title FROM movies WHERE Title ='"+title+"'")
    entries = querycur.fetchall()
    if len(entries)==0:
        ret_msg = "Movie " + title + " could not be updated- Movie doesn't exist in the database."
    else:
        msg = "UPDATE movies SET Year="+str(year)+", Director='"+director+"', Actor='"+actor+"', Release_Date='"+release_date+"', Rating="+str(rating)+" WHERE Title='"+title+"'"
        querycur.execute(msg)
        cnx.commit()
        ret_msg = "Movie with title " + title + " sucessfully updated."
    return hello(ret_msg)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    ret_msg = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    

    cur = cnx.cursor()
    cur.execute("SELECT Title FROM movies WHERE Title ='"+title+"'")
    rows = cur.fetchall()
    if len(rows) == 0:
        ret_msg = "Movie with title "+title+ " does not exist."
    else:
        cur.execute("DELETE FROM movies WHERE Title ='"+title+"'")
        cnx.commit()
        ret_msg = "Movie with title " + title + " successfully deleted."

    
    return hello(ret_msg)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    actor = request.args['search_actor']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    ret_msg = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    msg = "SELECT Title, Year, Actor FROM movies where Actor='"+actor+"'"
    cur = cnx.cursor()
    cur.execute(msg)
    rows = cur.fetchall()
    res = []
    if len(rows) ==0:
        ret_msg = 'No movies found for actor '+actor
    else:
        ret_msg = 'Results:'
        for row in rows:
            formatted_row = row[0] +', '+str(row[1])+', '+row[2]
            res.append(formatted_row)

    return hello(ret_msg, res)

@app.route('/highest_rating',methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    ret_msg = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    
    msg = "SELECT Title, Year, Actor, Director, Rating FROM movies WHERE Rating >= ALL(Select Rating From movies)"
    cur = cnx.cursor()
    cur.execute(msg)
    rows = cur.fetchall()
    res = []
    if len(rows) == 0:
        ret_msg = 'Database currently contains no movies.'
    else:
        ret_msg = 'Results:'
        for row in rows:
            formatted_row = row[0]+', '+str(row[1])+', '+row[2]+', '+row[3]+', '+str(row[4])
            res.append(formatted_row)

    return hello(ret_msg, res)

@app.route('/lowest_rating',methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    ret_msg = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    
    msg = "SELECT Title, Year, Actor, Director, Rating FROM movies WHERE Rating <= ALL(Select Rating From movies)"
    cur = cnx.cursor()
    cur.execute(msg)
    rows = cur.fetchall()
    res = []
    if len(rows) == 0:
        ret_msg = 'Database currently contains no movies.'
    else:
        ret_msg = 'Results: '
        for row in rows:
            formatted_row = row[0]+', '+str(row[1])+', '+row[2]+', '+row[3]+', '+str(row[4])
            res.append(formatted_row)

    return hello(ret_msg, res)


@app.route("/")
def hello(msg='', e=None):
    return render_template('index.html', message=msg, entries = e)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
